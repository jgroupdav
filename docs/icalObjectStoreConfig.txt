ICalendarObjectStore is configured with the following properties:

groupdav.server | setServer("http://server:port/") 
groupdav.store | setStore("/groupdav/Calendar")
groupdav.user | connect_plain(user, pass) (Requires OPTION_STOREDPASS)
groupdav.pass | connect_plain(user, pass) (Requires OPTION_STOREDPASS)