/*
 * JGroupDAV
 * CardDAVFormatter.java (C) 2008,2010 Mathew McBride
 * 
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 * 
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY Mathew McBride ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of Mathew McBride.
 */

package net.bionicmessage.groupdav;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 *
 * @author matt
 */
public class CardDAVFormatter implements DAVReportFormatter {
    public CardDAVFormatter() {

    }

    public byte[] formatReport(List<String> urlsToGet) throws UnsupportedEncodingException{
        StringBuffer reportRequest = new StringBuffer("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
        reportRequest.append("<C:addressbook-multiget xmlns:D=\"DAV:\"");
        reportRequest.append(" xmlns:C=\"urn:ietf:params:xml:ns:carddav\">\r\n");
        for (String str : urlsToGet) {
            reportRequest.append("<D:href>");
            reportRequest.append(str);
            reportRequest.append("</D:href>\r\n");
        }
        reportRequest.append("<D:prop><D:getetag/><C:address-data/></D:prop>");
        reportRequest.append("</C:addressbook-multiget>");
        byte[] data = reportRequest.toString().getBytes("UTF-8");
        return data;
    }

}
