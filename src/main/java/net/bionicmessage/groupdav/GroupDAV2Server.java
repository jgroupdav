/* GroupDAV2Server.java
 * Copyright 2010 Mathew McBride
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY Mathew McBride ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of Mathew McBride.
 */
package net.bionicmessage.groupdav;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.bionicmessage.extutils.Base64;
import net.bionicmessage.groupdav.http.HttpDelete;
import net.bionicmessage.groupdav.http.HttpPropfind;
import net.bionicmessage.groupdav.http.HttpReport;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

public class GroupDAV2Server
        implements DAVServer, IDAVHandler {

    private static final String DAV_PROPFIND = "<?xml version=\"1.0\" " +
            "encoding=\"utf-8\"?>\n<propfind xmlns=\"DAV:\"\n xmlns:I=\"urn:ietf:params:xml:ns:caldav\"\n" +
            " xmlns:C=\"urn:ietf:params:xml:ns:carddav\"\n" +
            " xmlns:G=\"http://groupdav.org/\"\n" +
            " xmlns:A=\"http://calendarserver.org/ns/\">\n\t" +
            "<prop>\n\t\t<displayname/>\n\t\t<resourcetype/>\n\t\t" +
            "<getetag/>\n\t\t<getcontenttype/>\n\t\t<current-user-privilege-set/>" +
            "\n\t\t<G:component-set/>\n\t\t<I:supported-calendar-component-set/>" +
            "\n\t\t<I:supported-calendar-data/>\n\t\t<I:calendar-description/>" +
            "\n\t\t<C:supported-address-data/>\n\t\t<C:addressbook-description/>" +
            "\n\t\t<A:getctag/>\n\t</prop>\n</propfind>\n"; 
    /* private static final String DAV_PROPFIND = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
            + "<propfind xmlns=\"DAV:\"><prop><getetag/></prop></propfind>"; */
    
    private URI serverURI = null;
    private DefaultHttpClient httpClient = null;
    private BasicHttpContext httpContext = null;
    private HttpHost httpHost = null;
    private SAXDAVHandler handler = null;
    boolean ready = false;
    private Exception exception = null;
    protected DAVReportFormatter formatter = null;

    public static final String HTTP_USER_AGENT_STRING =
            "bionicmessage.net JGroupDAV/2.0 (GroupDAV2Server)";
    public GroupDAV2Server() {
        this.httpClient = new DefaultHttpClient();
        this.httpContext = new BasicHttpContext();
        this.httpClient.getParams().setParameter(CoreProtocolPNames.USER_AGENT,
                HTTP_USER_AGENT_STRING);
    }

    public Map<String, String> listObjects(String url) throws Exception {
        URI pathURI = this.serverURI.resolve(url);
        HttpPropfind pf = new HttpPropfind(pathURI);
        StringEntity he = new StringEntity(DAV_PROPFIND);
        he.setContentType("text/xml");
        pf.setEntity(he);
        HttpResponse hr = this.httpClient.execute(this.httpHost, pf, this.httpContext);
        InputSource is = new InputSource(hr.getEntity().getContent());
        this.handler = new SAXDAVHandler(this, is);
        while (!(this.ready));
        this.ready = false;
        hr.getEntity().consumeContent();
        return this.handler.getEtagMap();
    }

    public List<GroupDAVObject> getObjects(String baseURL, final List<String> urls) throws Exception {
        // Do we support multiget?
        boolean supportMultiget = doesSupportMultiget(baseURL);
        ArrayList<GroupDAVObject> downloaded = new ArrayList(urls.size());
        ArrayList<String> downloadBatch = getBatch(urls, 500);
        while (downloadBatch.size() != 0) {
            if (supportMultiget) {
                try {
                    List<GroupDAVObject> batch = downloadMultipleObjects(baseURL, downloadBatch);
                    for (GroupDAVObject gdo : batch) {
                        downloaded.add(gdo);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                for (String s : downloadBatch) {
                    try {
                        GroupDAVObject gdo = downloadSingleObject(s);
                        downloaded.add(gdo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            downloadBatch = getBatch(urls, 500);
        }
        return downloaded;
    }

    public GroupDAVObject modifyObject(String url, String etag, String ctype, byte[] newContents)
            throws Exception {
        URI pathURI = this.serverURI.resolve(url);
        HttpPut put = new HttpPut(pathURI);
        put.addHeader("If-Match", etag);
        ByteArrayEntity entity = new ByteArrayEntity(newContents);
        entity.setContentType(ctype+";charset=UTF-8");
        //entity.setContentEncoding("UTF-8");
        put.setEntity(entity);
        HttpResponse hr = this.httpClient.execute(this.httpHost, put, this.httpContext);
        if (hr.getEntity() != null) {
            hr.getEntity().consumeContent();
        }
        if (hr.getStatusLine().getStatusCode() != 204) {
            return null;
        }
        GroupDAVObject gbo = new GroupDAVObject();
        gbo.setStatus(hr.getStatusLine().getStatusCode());
        return gbo;
    }

    public GroupDAVObject putObject(String url, String ctype, byte[] content) throws Exception {
        URI pathURI = this.serverURI.resolve(url);
        //pathURI = pathURI.resolve("new.ics");
        
        HttpPut put = new HttpPut(pathURI);
        ByteArrayEntity entity = new ByteArrayEntity(content);
        entity.setContentType(ctype+";charset=UTF-8");
        //entity.setContentEncoding("UTF-8");
        put.setEntity(entity);
        HttpResponse hr = this.httpClient.execute(this.httpHost, put, this.httpContext);
        if (hr.getEntity() != null) {
            hr.getEntity().consumeContent();
        }
        GroupDAVObject obj = new GroupDAVObject();
        Header loc = hr.getFirstHeader("Location");
        if (loc != null) {
            obj.setLocation(loc.getValue());
        } else {
            obj.setLocation(url);
        }
        return obj;
    }

    public boolean deleteObject(String url, String etag) throws Exception {
        URI pathURI = this.serverURI.resolve(url);
        HttpDelete delete = new HttpDelete(pathURI);
        delete.addHeader("If-Match", etag);
        HttpResponse hr = this.httpClient.execute(this.httpHost, delete, this.httpContext);
        if (hr.getEntity() != null) {
            hr.getEntity().consumeContent();
        }

        return (hr.getStatusLine().getStatusCode() == 204);
    }

    public void setProperty(String name, Object value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setAuthenticationBasic(String b64) {
        byte[] decoded = Base64.decode(b64);
        String up = new String(decoded);
        AuthScope as = new AuthScope(this.serverURI.getHost(), this.serverURI.getPort());
        UsernamePasswordCredentials upc = new UsernamePasswordCredentials(up);

        HttpHost targetHost =
                new HttpHost(this.serverURI.getHost(), this.serverURI.getPort());
        this.httpClient.getCredentialsProvider().setCredentials(as, upc);
        AuthCache authCache = new BasicAuthCache();
        BasicScheme basicAuth = new BasicScheme();
        authCache.put(targetHost, basicAuth);

// Add AuthCache to the execution context
        this.httpContext.setAttribute(ClientContext.AUTH_CACHE, authCache);
    }

    public void setAuthentication(String user, String pass) {
        AuthScope as = new AuthScope(this.serverURI.getHost(), this.serverURI.getPort());
        UsernamePasswordCredentials upc = new UsernamePasswordCredentials(user, pass);

        HttpHost targetHost =
                new HttpHost(this.serverURI.getHost(), this.serverURI.getPort());
        this.httpClient.getCredentialsProvider().setCredentials(as, upc);
        AuthCache authCache = new BasicAuthCache();
        BasicScheme basicAuth = new BasicScheme();
        authCache.put(targetHost, basicAuth);

// Add AuthCache to the execution context
        this.httpContext.setAttribute(ClientContext.AUTH_CACHE, authCache);
        
        this.httpClient.getCredentialsProvider().setCredentials(as, upc);
    }

    public void setServer(String server) throws URISyntaxException {
        this.serverURI = new URI(server).parseServerAuthority();
        int port = 80;
        if (this.serverURI.getPort() != -1) {
            port = this.serverURI.getPort();
        }
        if (("https".equals(this.serverURI.getScheme())) && (port == 80)) {
            port = 443;
        }

        this.httpHost = new HttpHost(this.serverURI.getHost(), port, this.serverURI.getScheme());
        AuthCache authCache = new BasicAuthCache();

        BasicScheme basicAuth = new BasicScheme();
        authCache.put(this.httpHost, basicAuth);

        BasicHttpContext localcontext = new BasicHttpContext();
        localcontext.setAttribute("http.auth.auth-cache", authCache);
    }

    public boolean getReady() {
        return this.ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public void setRetException(Exception e) {
        this.exception = e;
    }

    private ArrayList<String> getBatch(List<String> collection, int size) {
        ArrayList batch = new ArrayList(size);
        int i = 0;
        Iterator iterator = collection.iterator();
        while ((iterator.hasNext()) && (i != size)) {
            String url = (String) iterator.next();
            batch.add(url);
            iterator.remove();
            ++i;
        }
        return batch;
    }

    public GroupDAVObject downloadSingleObject(String url) throws Exception {
        URI pathURI = this.serverURI.resolve(url);
        HttpGet hg = new HttpGet(pathURI);
        HttpResponse hr = this.httpClient.execute(this.httpHost, hg, this.httpContext);
        HttpEntity he = hr.getEntity();
        byte[] data = EntityUtils.toByteArray(he);
        Header etag = hr.getFirstHeader("ETag");
        Header loc = hr.getFirstHeader("Location");
        GroupDAVObject gdo = new GroupDAVObject();
        if (loc != null) {
            URI newLocation = this.serverURI.resolve(loc.getValue());
            gdo.setLocation(newLocation.getPath());
        } else {
            gdo.setLocation(pathURI.getPath());
        }
        gdo.setEtag(etag.getValue());
        gdo.setContent(data);
        hr.getEntity().consumeContent();
        return gdo;
    }


    public List<GroupDAVObject> downloadMultipleObjects(String url, List<String> urls) throws Exception {
        URI pathURI = this.serverURI.resolve(url);
        HttpReport hr = new HttpReport(pathURI);
        hr.setHeader("Depth", "1");
        ByteArrayEntity entity = new ByteArrayEntity(formatter.formatReport(urls));
        entity.setContentType("text/xml;charset=UTF-8");
        //entity.setContentEncoding("UTF-8");
        hr.setEntity(entity);
        // Send DAV Report
        HttpResponse hresponse = this.httpClient.execute(this.httpHost, hr, this.httpContext);
        
        InputSource is = new InputSource(hresponse.getEntity().getContent());
        this.handler = new SAXDAVHandler(this, is);
        while (!(this.ready));
        this.ready = false;
        hresponse.getEntity().consumeContent();
        Stack<String> contents = this.handler.getContents();
        Stack<String> objectURLs = this.handler.getHrefs();
        Stack<String> etags = this.handler.getEtags();
        List<GroupDAVObject> objects = new ArrayList(etags.size());
        while(!objectURLs.empty()) {
            String objectURL = objectURLs.pop();
            String content = contents.pop();
            String etag = etags.pop();
            GroupDAVObject gdo = new GroupDAVObject();
            gdo.setContent(content.getBytes());
            gdo.setEtag(etag);
            gdo.setLocation(objectURL);
            objects.add(gdo);
        }
        return objects;
    }

    public void setFormatter(DAVReportFormatter formatter) {
        this.formatter = formatter;
    }

    public boolean doesSupportMultiget(String path) throws IOException {
        boolean supportMultiget = false;
        URI pathURI = this.serverURI.resolve(path);
        HttpOptions hr = new HttpOptions(pathURI);
        hr.setHeader("Depth","1");
        HttpResponse response = this.httpClient.execute(this.httpHost, hr, this.httpContext);
        Header optionsHeader = response.getFirstHeader("DAV");
        if (optionsHeader != null) {
            String supported = optionsHeader.getValue();
            if (supported.contains("addressbook") ||
                    supported.contains("calendar-access")) {
                supportMultiget = true;
            }
        }
        if (response.getEntity() != null) {
            response.getEntity().consumeContent();
        }
        return supportMultiget;
    }
    
}
