/*
 * JGroupDAV library
 * SAXDAVHandler.java created 19th October 2008
 * Copyright 2008-2010 Mathew McBride
 * 
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY Mathew McBride ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of Mathew McBride.
 */
package net.bionicmessage.groupdav;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/** A SAX handler class for Cal/Card/GroupDAV PROPFIND
 * and REPORT operations.
 * @author matt
 */
public class SAXDAVHandler extends DefaultHandler {

    protected Stack<String> hrefs;
    protected Stack<String> etags;
    protected Stack<String> contents;
    protected Stack<String> types;
    protected StringBuffer chars;
    protected IDAVHandler callback;
    protected Map<String, String> cachedEtagMap;

    /** 
     * Creates a handler
     * @param parent
     */
    public SAXDAVHandler(IDAVHandler parent) {
        hrefs = new Stack();
        etags = new Stack();
        contents = new Stack();
        types = new Stack();
        callback = parent;
    }

    /** Creates a handler and handles the parsing of XML from the provided source */
    public SAXDAVHandler(IDAVHandler parent, InputSource source) throws Exception {
        this(parent);
        XMLReader p = XMLReaderFactory.createXMLReader();
        p.setContentHandler(this);
        p.parse(source);
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        callback.setReady(false);
    }

    @Override
    public void startElement(String namespaceURI, String localName,
            String qualifiedName, Attributes atts) throws SAXException {
        super.startElement(namespaceURI, localName, localName, atts);
        chars = new StringBuffer();
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (localName.equals("getetag")) {
            etags.push(chars.toString());
        } else if (localName.equals("href")) {
            hrefs.push(chars.toString());
        } else if (localName.equals("address-data") || localName.equals("calendar-data")) {
            // replace '\n' with '\r\n' because Xerces wants to remove it. BAD BAD
            String ch = chars.toString();
            if (ch.contains("\n") && !ch.contains("\r")) {
                ch = ch.replace("\n", "\r\n");
            }
            contents.push(ch);
        }
        if (localName.contains("collection") && uri.contains("groupdav.org")) {
            types.push(localName);
        } else if (localName.contains("getcontenttype")) {
            types.push("obj");
        } else if (localName.contains("response")) {
            if (hrefs.size() != etags.size()) {
                etags.push("");
            }
            if (types.size() != etags.size()) {
                types.push("");
            }
        }
        super.endElement(uri, localName, qName);
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        chars.append(ch, start, length);
    }

    @Override
    public void endDocument() throws SAXException {
        super.endDocument();
        callback.setReady(true);
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        callback.setReady(true);
        callback.setRetException(e);
    }

    public Stack<String> getContents() {
        return contents;
    }

    public Stack<String> getEtags() {
        return etags;
    }

    public Stack<String> getHrefs() {
        return hrefs;
    }

    public Stack<String> getTypes() {
        return types;
    }

    public Map<String, String> getEtagMap() {
        if (cachedEtagMap == null) {
            cachedEtagMap = new HashMap(etags.size());
            for (int i = 0; i < hrefs.size(); i++) {
                String href = hrefs.get(i);
                String etag = etags.get(i);
                cachedEtagMap.put(href, etag);
            }
        }
        return cachedEtagMap;
    }
}
