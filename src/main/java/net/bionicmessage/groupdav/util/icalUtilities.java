/* BionicMessage.net Java GroupDAV library
 *
 *
 * Created on Saturday 16th May, 2009
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 *
 */
package net.bionicmessage.groupdav.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Dur;
import net.fortuna.ical4j.model.TimeZoneRegistryImpl;
import net.fortuna.ical4j.model.property.DateProperty;
import net.fortuna.ical4j.model.property.DtEnd;
import net.fortuna.ical4j.model.property.DtStart;
import net.fortuna.ical4j.model.property.Duration;

/**
 *
 * @author matt
 */
public class icalUtilities {

    private static TimeZoneRegistryImpl tzRegistry = null;

    /** This function attempts to attach UTC time to all day events
     * in order to bring in line with eGroupware. */
    public static DateProperty correctAllDayTimezones(DateProperty in) {
        DateProperty modified = in;
        if (modified != null && modified.toString().length() == 8) {
            modified.setUtc(true);
            return modified;
        }
        return in;
        /* getRegistry();
        TimeZone utc = tzRegistry.getTimeZone("UTC");
        modified.setTimeZone(utc);
        return modified; */

    }

    public static List getDtStartEnd(Component vc) {
        ArrayList toReturn = new ArrayList();
        Component vcal = vc;
        DtStart dts = (DtStart) vcal.getProperty("DTSTART");
        DtEnd dte = (DtEnd) vcal.getProperty("DTEND");
        Duration dur = (Duration) vcal.getProperty("DURATION");
        Timestamp ds = null;
        Timestamp de = null;
        if (dts != null) {
            dts = (DtStart) icalUtilities.correctAllDayTimezones(dts);
            ds = new Timestamp(dts.getDate().getTime());
        }
        // For events that have DURATION, we convert to DTEND
        if (dte == null && dur != null && dts != null) {
            java.util.Calendar dtc = java.util.Calendar.getInstance();
            Dur durDetail = dur.getDuration();
            dtc.setTime(ds);
            dtc.add(java.util.Calendar.WEEK_OF_YEAR, durDetail.getWeeks());
            dtc.add(java.util.Calendar.DATE, durDetail.getDays());
            dtc.add(java.util.Calendar.HOUR, durDetail.getHours());
            dtc.add(java.util.Calendar.MINUTE, durDetail.getMinutes());
            dtc.add(java.util.Calendar.SECOND, durDetail.getSeconds());
            net.fortuna.ical4j.model.DateTime dtedate =
                    new net.fortuna.ical4j.model.DateTime(dtc.getTime());
            dte = new DtEnd(dtedate);
            dte.setTimeZone(dts.getTimeZone());
            vcal.getProperties().add(dte);
            vcal.getProperties().remove(dur);
        } else if (dte == null && dts != null) { // Another hack, for mysterious events lacking DTEND
            dte = new DtEnd(dts.getDate());
            dte.setTimeZone(dte.getTimeZone());
        }
        if (dte != null) {
            dte = (DtEnd) icalUtilities.correctAllDayTimezones(dte);
            de = new Timestamp(dte.getDate().getTime());
        }
        toReturn.add(ds);
        toReturn.add(de);
        toReturn.add(vcal);
        return toReturn;
    }

    private static void getRegistry() {
        if (tzRegistry == null) {
            tzRegistry = new TimeZoneRegistryImpl();
        }
    }
}
