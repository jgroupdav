/*
 * ICalendarObjectStore.java
 *
 * Created on 26 August 2006, 14:56
 * Copyright (c) 2006-2008 Mathew McBride / "BionicMessage.net"
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 *
 */

package net.bionicmessage.objects;

/**
 * Contains constants for use with net.bionicmessage.objects
 * @author matt
 */
public class StoreConstants {
/** Base property for store location */
    public static final String PROPERTY_STORE_LOCATION = "store.location";
    /** Properties name for server */
    public static final String PROPERTY_SERVER = "store.server";
    /** Properties name for user */
    public static final String PROPERTY_USER = "store.user";
    /** Properties name for password */
    public static final String PROPERTY_PASSWORD = "store.password";
    /** Base property for sources */
    public static final String BASE_PROPERTY_SOURCE = "store.source.";
    /** CalDAV compare property name */
    public static final String CALDAV_COMPARE_PROPERTY = "store.cdav.property";
    /** CalDAV type property name */
    public static final String CALDAV_COMPARE_COMPONENT = "store.cdav.component";
    /** CalDAV compare start time */
    public static final String CALDAV_COMPARE_START = "store.cdav.start";
    /** CalDAV compare end time */
    public static final String CALDAV_COMPARE_END = "store.cdav.end";
    /** Override the server mode detected by JGroupDAV */
    public static final String PROPERTY_SERVER_MODE = "store.server.mode";
    /** How many downloader threads to run with a GroupDAV server */
    public static final String PROPERTY_SERVER_CTHREADS = "store.server.cthreads";
    /** How many items to download at once (CalDAV/GroupDAV 2) */
    public static final String PROPERTY_SERVER_ITEMS = "store.server.items";
}
