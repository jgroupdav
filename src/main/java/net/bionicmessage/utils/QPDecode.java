/* JGroupDAV
 * (C) 2009 Mathew McBride
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 */

package net.bionicmessage.utils;

import java.util.List;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Parameter;
import net.fortuna.ical4j.model.Property;
import org.apache.commons.codec.net.QuotedPrintableCodec;

/**
 *
 * @author matt
 */
public class QPDecode {
    public static final Calendar decodeQP(Calendar in) {
        QuotedPrintableCodec qpc = new QuotedPrintableCodec();
        Calendar cal = in;
        List<Component> calComponents = (List<Component>)cal.getComponents();
        for(Component c : calComponents) {
            List<Property> calProperties = (List<Property>)c.getProperties();
            for(Property p: calProperties) {
                Parameter encodingParameter = p.getParameter("ENCODING");
                if (encodingParameter != null 
                        && "QUOTED-PRINTABLE".equals(encodingParameter.getValue())) {
                    String value = p.getValue(); 
                    try {
                    value = qpc.decode(value);
                    p.setValue(value);
                    p.getParameters().remove(encodingParameter);
                    } catch (Exception e) {
                        System.err.println("ERROR: Cannot QP decode: "+value);
                    }
                }
            }
        }
        return cal;
    }
}
