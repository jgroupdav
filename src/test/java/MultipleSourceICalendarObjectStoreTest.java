/*
 * MultipleSourceICalendarObjectStoreTest.java
 * JUnit 4.x based test
 *
 * Created on August 5, 2007, 11:35 AM
 *
 * Copyright 2007-2010 Mathew McBride
 */


import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.DtEnd;
import net.fortuna.ical4j.model.property.DtStart;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import net.bionicmessage.objects.*;
import net.fortuna.ical4j.model.property.Summary;
/**
 *
 * @author matt
 */
public class MultipleSourceICalendarObjectStoreTest {
    
    public static final String TEST_PROPERTIES_FILE = 
            ".multiplesource_icalobject_test_props";
    MultipleSourceICalendarObjectStore ico = null;
    Properties testProps = null;
    File stPath = null;
    String uidadded = null;
    File testFile = null;
    public MultipleSourceICalendarObjectStoreTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
       ico = null;
        // Load a properties file from the local user dir
        String testPropsPath = System.getProperty("user.home")+
                System.getProperty("file.separator")+
                TEST_PROPERTIES_FILE;
	testFile = new File(testPropsPath);
	if (!testFile.exists()) {
		System.err.printf("~/%s has not been set up. Exiting..\n",testPropsPath);
                System.err.print("Perhaps you should build with -Dmaven.test.skip.exec=true ?");
		System.err.flush();
		System.exit(1);
	}
        testProps = new Properties();
        testProps.load(new FileInputStream(testFile));
        String storePath = testProps.getProperty(StoreConstants.PROPERTY_STORE_LOCATION);
        stPath = new File(storePath);
        boolean create = stPath.mkdirs();
        int options = 0;
        ico = new MultipleSourceICalendarObjectStore(storePath ,options);
        ico.setProperties(testProps);
        ico.loadSourcesFromProps();
        ico.setServerFromProperties();
    }

    @After
    public void tearDown() throws Exception {
       
    }
    @Test
    public void test_connectAndSyncItems() throws Exception {
            ico.startSync();
            ico.printDebugReport();       
        ico.close();
    }
    @Test
    public void test_connectAndSyncItemsAgain() throws Exception {
            ico.startSync();
            ico.printDebugReport();
        ico.close();
    }
    /* In the tests below, avoid subsequent test reliance */
    @Test
    public void reconnectAndAddItemThenDeleteIt() throws Exception {
        this.setUp();
        File firstSample = new File ("docs/icalsamples/10350.ics");
        CalendarBuilder cbuild = new CalendarBuilder();
        Calendar sample = cbuild.build(new FileInputStream(firstSample));
        ico.startSync();
        VEvent ve = (VEvent) sample.getComponent(VEvent.VEVENT);
        DtStart dts = (DtStart) ve.getProperty(DtStart.DTSTART);
        long start = dts.getDate().getTime() / 1000;
        DtEnd dte = (DtEnd) ve.getProperty(DtEnd.DTEND);
        long end = dte.getDate().getTime() / 1000;
        String uid = ve.getUid().getValue();
        String summary = ve.getSummary().getValue();
        uidadded = ico.addObject("default",uid,summary, sample.toString(), start, end);
        ico.printDebugReport();
        ico.close();
        // Start again
        this.setUp();
        ico.setServerFromProperties();
        ico.startSync();
        ico.deleteObject(uidadded);
        ico.printDebugReport();
        ico.close();
    }

    public void reconnectAndAddItemThenUpdateThenDeleteIt() throws Exception {
        this.setUp();
        File firstSample = new File ("docs/icalsamples/10350.ics");
        CalendarBuilder cbuild = new CalendarBuilder();
        Calendar sample = cbuild.build(new FileInputStream(firstSample));
        ico.startSync();
        VEvent ve = (VEvent) sample.getComponent(VEvent.VEVENT);
        DtStart dts = (DtStart) ve.getProperty(DtStart.DTSTART);
        long start = dts.getDate().getTime() / 1000;
        DtEnd dte = (DtEnd) ve.getProperty(DtEnd.DTEND);
        long end = dte.getDate().getTime() / 1000;
        String uid = ve.getUid().getValue();
        String summary = ve.getSummary().getValue();
        uidadded = ico.addObject("default",uid,summary, sample.toString(), start, end);
        ico.printDebugReport();
        ico.close();
        // Start up again
    }
    
    @Test
    public void testSearchUIDs() throws Exception{
        ico.startSync();
        List<String> uids = ico.getUIDList();
        for(String uid: uids) {
            Calendar c = ico.getObjectFromStore(uid);
            VEvent ve = (VEvent) c.getComponents(VEvent.VEVENT).get(0);
            Summary summ = ve.getSummary();
            DtStart dts = ve.getStartDate();
            assertNotNull(dts.getDate());
            DtEnd dte = ve.getEndDate();
            assertNotNull(dte.getDate());
            String matchingUid = ico.searchUids(summ.getValue(),
                    dts.getDate().getTime(), 
                    dte.getDate().getTime());
            System.out.printf("Trying to match UID: %s\n", uid);
            System.out.printf("Start date: %d, End date: %d\n",dts.getDate().getTime(),
                    dte.getDate().getTime());
            assertEquals(uid,matchingUid);
        }
        ico.close();
    }
    
}
