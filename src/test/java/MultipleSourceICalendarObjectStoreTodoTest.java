
import net.bionicmessage.objects.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.component.VToDo;
import net.fortuna.ical4j.model.property.Summary;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matt
 */
public class MultipleSourceICalendarObjectStoreTodoTest {

    public static final String TEST_PROPERTIES_FILE =
            ".multiplesource_icaltodo_test_props";
    MultipleSourceICalendarObjectStore ico = null;
    Properties testProps = null;
    File stPath = null;
    String uidadded = null;
    File testFile = null;
    public MultipleSourceICalendarObjectStoreTodoTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        ico = null;
        // Load a properties file from the local user dir
        String testPropsPath = System.getProperty("user.home") +
                System.getProperty("file.separator") +
                TEST_PROPERTIES_FILE;
		testFile = new File(testPropsPath);
		if (!testFile.exists()) {
		System.err.printf("~/%s has not been set up. Exiting. ",
			TEST_PROPERTIES_FILE);
		System.err.flush();
		System.exit(0);
		}
        testProps = new Properties();
        testProps.load(new FileInputStream(testFile));
        String storePath = testProps.getProperty(StoreConstants.PROPERTY_STORE_LOCATION);
        stPath = new File(storePath);
        boolean create = stPath.mkdirs();
        int options = MultipleSourceICalendarObjectStore.OPTION_TODO;
        ico = new MultipleSourceICalendarObjectStore(storePath, options);
        ico.setProperties(testProps);
        ico.setServerFromProperties();
        ico.loadSourcesFromProps();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test_connectAndSyncItems() throws Exception {
        ico.startSync();
        ico.printDebugReport();
        ico.close();
    }

    @Test
    public void testSearchUIDs() throws Exception{
        ico.startSync();
        List<String> uids = ico.getUIDList();
        for(String uid: uids) {
            Calendar c = ico.getObjectFromStore(uid);
            VToDo ve = (VToDo) c.getComponents(VToDo.VTODO).get(0);
            Summary summ = ve.getSummary();
            String matchingUid = ico.searchUids(summ.getValue(),
                    0,
                    0);
            System.out.printf("Trying to match UID: %s\n", uid);
            assertEquals(uid,matchingUid);
        }
        ico.close();
    }
}